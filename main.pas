﻿unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ASGSQLite3, DB, DBClient, Grids, DBGrids, DBCtrls,
  Mask, DateUtils;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    backBtn: TButton;
    nextBtn: TButton;
    Panel2: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    openDBRbtn: TRadioButton;
    newDBRbtn: TRadioButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    LiteDB: TASQLite3DB;
    LiteQuery: TASQLite3Query;
    Bevel2: TBevel;
    Label3: TLabel;
    authorEdt: TEdit;
    dbNameEdt: TEdit;
    lastChangeYearEdt: TEdit;
    providerMemo: TMemo;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    lastChangeMonthEdt: TEdit;
    lastChangeDayEdt: TEdit;
    calTypeEdt: TComboBox;
    Bevel3: TBevel;
    Label1: TLabel;
    LiteTableEvents: TASQLite3Table;
    Bevel1: TBevel;
    Label9: TLabel;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    DBNavigator1: TDBNavigator;
    LiteTableDefinition: TASQLite3Table;
    Button3: TButton;
    LiteTableEventsmonth: TStringField;
    LiteTableEventsday: TStringField;
    LiteTableEventsname: TMemoField;
    LiteTableEventsdescription: TMemoField;
    LiteTableEventsholiday: TMemoField;
    LiteTableEventsid: TIntegerField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBComboBox1: TDBComboBox;
    Label14: TLabel;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure nextBtnClick(Sender: TObject);
    procedure backBtnClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure LiteTableEventsnameGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure LiteTableEventsdescriptionGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure LiteTableEventsholidayGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure Button3Click(Sender: TObject);
    procedure LiteTableEventsholidaySetText(Sender: TField; const Text: string);
  private
    { Private declarations }
    procedure btnsAvailability();
  public
    { Public declarations }
    function createNewDB(fileName : WideString) : Boolean;
    procedure loadDBDefinitions();
    procedure loadEvents();
    procedure SaveDbDef();
    function SaveData(wantExit:Boolean = False): Boolean;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
uses
  memoedit;

var
    ncDefsName : array[0..4] of string = ('AUTHOR', 'DB_NAME', 'LAST_UPDATE', 'PROVIDER', 'CALENDAR');
    ncDefsValue : array[0..4] of string = ('MN', 'name','28.12.2007', 'http://khone.ir', 'JAL');


procedure TForm1.backBtnClick(Sender: TObject);
begin

{  if MessageDlg( 'در صورت ذخیره نکردن اطلاعات، با این کار ممکن است اطلاعات شما از دست برود،'#13+
              'آیا برای بازگشت اطمینان دارید؟', mtWarning, mbYesNo, 0, mbNo) = mrYes then
  begin
  }
    SaveData();
    
    if Panel2.Visible then
    begin
      Panel2.Visible := False;
      Panel1.Visible := True;
    end
    else if Panel3.Visible then
    begin

      loadDBDefinitions;

      Panel3.Visible := False;
      Panel2.Visible := True;
    end;

    btnsAvailability;
 // end;

end;

procedure TForm1.btnsAvailability;
begin
  backBtn.Enabled := True;
  nextBtn.Enabled := True;

  if Panel1.Visible then
    backBtn.Enabled := False
  else if Panel3.Visible then
    nextBtn.Enabled := False;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if SaveData(True) then
    Application.Terminate;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
    loadDBDefinitions;

end;

procedure TForm1.Button3Click(Sender: TObject);
begin

  lastChangeYearEdt.Text := IntToStr(YearOf(Now));
  lastChangeMonthEdt.Text := IntToStr(MonthOf(Now));
  lastChangeDayEdt.Text := IntToStr(DayOf(Now));

end;

function TForm1.createNewDB(fileName: WideString) : Boolean;
var
  i: Integer;
begin

  Result := False;
  
  if FileExists(fileName) then
    DeleteFile(fileName);
  LiteDB.Database := fileName;
  LiteDB.Open;
  LiteQuery.SQL.Text := 'CREATE TABLE [db_definition] ('+
                        '[name] VARCHAR(20)  UNIQUE NOT NULL PRIMARY KEY,'+
                        '[value] TEXT  NOT NULL'+
                        ');';
  try
    LiteQuery.Open;
  except
    ShowMessage('در ایجاد  دیتابیس مشکلی پیش آمد!');
    Exit;
  end;

  LiteQuery.SQL.Text := 'CREATE TABLE [events] ('+
                        '[id] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,'+
                        '[month] INTEGER(2) DEFAULT "1" NULL,'+
                        '[day] INTEGER(2) DEFAULT "0" NULL,'+
                        '[name] TEXT  NULL,'+
                        '[description] TEXT  NULL,'+
                        '[holiday] TEXT DEFAULT ''N'' NULL'+
                        ');';
  try
    LiteQuery.Open;
  except
    ShowMessage('در ایجاد  دیتابیس مشکلی پیش آمد!');
    Exit;
  end;
                                     
  LiteQuery.Close;                      

  for i := 0 to Length(ncDefsName) - 1 do
  begin
    LiteQuery.SQL.Text := 'INSERT INTO db_definition (`name`,`value`) VALUES ("'+
      ncDefsName[i]
      +'","'+
      ncDefsValue[i]
      +'")';
    try
      LiteQuery.Open;
    except
      ShowMessage('در بازکردن دیتابیس مشکلی پیش آمد!');
      Exit;
    end;
  end;

  LiteDB.Close;
  Result := True;
end;

procedure TForm1.DBGrid1DblClick(Sender: TObject);
begin
{    if (DBGrid1.SelectedField = LiteTableEventsname)
        or (DBGrid1.SelectedField = LiteTableEventsdescription)
        or (DBGrid1.SelectedField = LiteTableEventsholiday) then
    with TMemoEditorForm.Create(nil) do
    try
      DBMemoEditor.Text := DBGrid1.SelectedField.AsString;
      if ShowModal = mrOk then
      begin
        LiteTableEvents.Edit;
        DBGrid1.SelectedField.AsString := DBMemoEditor.Text;
      end;
    finally
      Free;
    end;
}
end;

procedure TForm1.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  if Key = VK_RETURN then
  begin
    if (DBGrid1.SelectedField = LiteTableEventsname)
        or (DBGrid1.SelectedField = LiteTableEventsdescription)
        or (DBGrid1.SelectedField = LiteTableEventsholiday) then
    with TMemoEditorForm.Create(nil) do
    try
      DBMemoEditor.Text := DBGrid1.SelectedField.AsString;
      if ShowModal = mrOk then
      begin
        LiteTableEvents.Edit;
        DBGrid1.SelectedField.AsString := DBMemoEditor.Text;
        LiteTableEvents.Post;
      end;
    finally
      Free;
    end;
  end;

  }
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin

  if not SaveData(True) then
    CanClose := False;

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  SysLocale.FarEast := True;
end;

procedure TForm1.LiteTableEventsdescriptionGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := Copy(LiteTableEventsdescription.AsString, 1, 50);
end;

procedure TForm1.LiteTableEventsholidayGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  if LowerCase(Copy(LiteTableEventsholiday.AsString, 1, 2)) = 'y' then
    Text := 'بله'
  else
    Text := 'خیر';

end;

procedure TForm1.LiteTableEventsholidaySetText(Sender: TField;
  const Text: string);
begin
  if Text = 'بله' then
    LiteTableEventsholiday.AsString := 'y'
  else
    LiteTableEventsholiday.AsString := 'n';

end;

procedure TForm1.LiteTableEventsnameGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := Copy(LiteTableEventsname.AsString, 1, 50);
end;

procedure TForm1.loadDBDefinitions;
var
  tmpStr : string;
begin
  if not LiteDB.Connected then
    Exit;

  LiteQuery.SQL.Text := 'SELECT * FROM db_definition';
  LiteQuery.Open;
  LiteQuery.First;
  DataSource1.Enabled := False;
  
  if not LiteQuery.IsEmpty then
    while not LiteQuery.Eof do
    begin
      if LiteQuery.FieldByName('name').AsString = 'AUTHOR' then
        authorEdt.Text := LiteQuery.FieldByName('value').AsString
        
      else if LiteQuery.FieldByName('name').AsString = 'DB_NAME' then
        dbNameEdt.Text := LiteQuery.FieldByName('value').AsString
        
      else if LiteQuery.FieldByName('name').AsString = 'PROVIDER' then
        providerMemo.Lines.Text := LiteQuery.FieldByName('value').AsString

      else if LiteQuery.FieldByName('name').AsString = 'LAST_UPDATE' then
      begin
        tmpStr := LiteQuery.FieldByName('value').AsString;
        lastChangeDayEdt.Text := copy(tmpStr, 1, pos('.', tmpStr) - 1);
        Delete(tmpStr, 1, Pos('.', tmpStr));
        lastChangeMonthEdt.Text := copy(tmpStr, 1, pos('.', tmpStr) - 1);
        Delete(tmpStr, 1, Pos('.', tmpStr));
        lastChangeYearEdt.Text := tmpStr;
      end

      else if LiteQuery.FieldByName('name').AsString = 'CALENDAR' then
        if LowerCase(LiteQuery.FieldByName('value').AsString) = 'jal' then
          calTypeEdt.ItemIndex := 0
        else if LowerCase(LiteQuery.FieldByName('value').AsString) = 'hij' then
          calTypeEdt.ItemIndex := 1
        else if LowerCase(LiteQuery.FieldByName('value').AsString) = 'gre' then
          calTypeEdt.ItemIndex := 2
      ;

      LiteQuery.Next;
    end;
      
end;

procedure TForm1.loadEvents;
begin

  LiteTableEvents.Close;
  LiteTableEvents.TableName := 'events';
  try
    LiteTableEvents.Open;
    DataSource1.Enabled := True;
    
  except
    ShowMessage('خطا در باز کردن دیتابیس');
    Exit;
  end;   

end;

procedure TForm1.nextBtnClick(Sender: TObject);
begin

  if Panel1.Visible then
  begin
  
    if newDBRbtn.Checked then
    begin
      if not SaveDialog1.Execute() then
        Exit;
      //create new db
      if not createNewDB(SaveDialog1.FileName) then
        Exit;
      LiteDB.Database := SaveDialog1.FileName;
        
    end
    else
    begin
      if not OpenDialog1.Execute then
        Exit;
      LiteDB.Database := OpenDialog1.FileName;
      
    end;

    try
      LiteDB.Open;
    except
      ShowMessage('خطا در باز کردن دیتابیس');
      Exit;
    end;

    //chk db validation here <<<<<=========

    loadDBDefinitions;

    Panel1.Visible := False;
    Panel2.Visible := True;
  end
  else if Panel2.Visible then
  begin
{    if MessageDlg( 'در صورت ذخیره نکردن اطلاعات، با این کار ممکن است اطلاعات شما از دست برود،'#13+
                'آیا برای ادامه اطمینان دارید؟', mtWarning, mbYesNo, 0, mbNo) = mrNo then
      Exit;
}
    SaveData();
    loadEvents;

    Panel2.Visible := False;
    Panel3.Visible := True;
  end;
          
  btnsAvailability;
end;

procedure TForm1.SaveDbDef;
var
  tmpStr : WideString;
begin

  try
    LiteTableDefinition.TableName := 'db_definition';
    LiteTableDefinition.Open;
    if not LiteTableDefinition.IsEmpty then
    begin
      LiteTableDefinition.First;

      case calTypeEdt.ItemIndex of
        1 : tmpStr := 'HIJ';
        2 : tmpStr := 'GRE';
        0 : tmpStr := 'JAL';
      end;

      while not LiteTableDefinition.Eof do
      begin
        LiteTableDefinition.Edit;

        if LiteTableDefinition.FieldByName('name').AsString = 'AUTHOR' then
          LiteTableDefinition.FieldByName('value').AsString := authorEdt.Text

        else if LiteTableDefinition.FieldByName('name').AsString = 'DB_NAME' then
          LiteTableDefinition.FieldByName('value').AsString := dbNameEdt.Text

        else if LiteTableDefinition.FieldByName('name').AsString = 'PROVIDER' then
          LiteTableDefinition.FieldByName('value').AsString := providerMemo.Lines.Text

        else if LiteTableDefinition.FieldByName('name').AsString = 'LAST_UPDATE' then
          LiteTableDefinition.FieldByName('value').AsString :=
            lastChangeDayEdt.Text+'.'+lastChangeMonthEdt.Text+'.'+lastChangeYearEdt.Text

        else if LiteTableDefinition.FieldByName('name').AsString = 'CALENDAR' then
          LiteTableDefinition.FieldByName('value').AsString := tmpStr;

        if LiteTableDefinition.State = dsEdit then
          LiteTableDefinition.Post;
        LiteTableDefinition.Next;
      end;
    end;

  except
    ShowMessage('در ذخیره سازی مشکلی پیش آمد.');

  end;
end;

function TForm1.SaveData(wantExit:Boolean): Boolean;
begin
  Result := False;

  if wantExit then
    if MessageDlg('آیا برای خروج اطمینان دارید؟', mtWarning, mbYesNo, 0, mbNo) = mrNo then
    Exit;

  if Panel2.Visible then
    SaveDbDef

  else if Panel3.Visible then
    if (DataSource1.State = dsEdit) then
      LiteTableEvents.Post;

  Result := True;

end;

end.
