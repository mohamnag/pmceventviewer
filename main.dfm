object Form1: TForm1
  Left = 0
  Top = 0
  BiDiMode = bdRightToLeft
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Events DB Creator For Persian Multi Calendar'
  ClientHeight = 361
  ClientWidth = 556
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  ParentBiDiMode = False
  Position = poDesktopCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 539
    Height = 314
    TabOrder = 0
    object Label2: TLabel
      Left = 443
      Top = 70
      Width = 55
      Height = 13
      Caption = #1575#1606#1578#1582#1575#1576' '#1705#1606#1610#1583':'
    end
    object Bevel3: TBevel
      Left = 11
      Top = 41
      Width = 513
      Height = 2
      Shape = bsBottomLine
    end
    object Label1: TLabel
      Left = 457
      Top = 16
      Width = 67
      Height = 19
      Caption = #1605#1585#1581#1604#1607' '#1575#1608#1604
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object openDBRbtn: TRadioButton
      Left = 193
      Top = 112
      Width = 265
      Height = 17
      Caption = #1576#1575#1586' '#1705#1585#1583#1606' '#1601#1575#1610#1604#1610' '#1705#1607' '#1602#1576#1604#1575' '#1575#1610#1580#1575#1583' '#1588#1583#1607' '#1608' '#1575#1583#1575#1605#1607' '#1608#1610#1585#1575#1610#1588
      TabOrder = 0
    end
    object newDBRbtn: TRadioButton
      Left = 345
      Top = 89
      Width = 113
      Height = 17
      Caption = #1575#1610#1580#1575#1583' '#1601#1575#1610#1604' '#1580#1583#1610#1583
      Checked = True
      TabOrder = 1
      TabStop = True
    end
  end
  object Panel3: TPanel
    Left = 8
    Top = 8
    Width = 539
    Height = 314
    TabOrder = 5
    Visible = False
    object Bevel1: TBevel
      Left = 11
      Top = 41
      Width = 513
      Height = 2
      Shape = bsBottomLine
    end
    object Label9: TLabel
      Left = 446
      Top = 16
      Width = 78
      Height = 19
      Caption = #1605#1585#1581#1604#1607' '#1587#1608#1605
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 277
      Top = 78
      Width = 23
      Height = 13
      Caption = #1588#1585#1581
    end
    object Label11: TLabel
      Left = 499
      Top = 53
      Width = 14
      Height = 13
      Caption = #1605#1575#1607
    end
    object Label12: TLabel
      Left = 499
      Top = 78
      Width = 26
      Height = 13
      Caption = #1593#1606#1608#1575#1606
    end
    object Label13: TLabel
      Left = 426
      Top = 53
      Width = 13
      Height = 13
      Caption = #1585#1608#1586
    end
    object Label14: TLabel
      Left = 499
      Top = 179
      Width = 29
      Height = 13
      Caption = #1578#1593#1591#1610#1604
    end
    object DBGrid1: TDBGrid
      Left = 18
      Top = 203
      Width = 506
      Height = 103
      DataSource = DataSource1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      OnKeyDown = DBGrid1KeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'month'
          Title.Caption = #1605#1575#1607
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'day'
          Title.Caption = #1585#1608#1586
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'name'
          Title.Caption = #1593#1606#1608#1575#1606' '#1585#1608#1610#1583#1575#1583
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'description'
          Title.Caption = #1588#1585#1581' '#1585#1608#1610#1583#1575#1583
          Width = 195
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'holiday'
          Title.Caption = #1578#1593#1591#1610#1604
          Width = 35
          Visible = True
        end>
    end
    object DBNavigator1: TDBNavigator
      Left = 18
      Top = 47
      Width = 240
      Height = 25
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit1: TDBEdit
      Left = 467
      Top = 51
      Width = 25
      Height = 21
      DataField = 'month'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Left = 394
      Top = 51
      Width = 26
      Height = 21
      DataField = 'day'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBMemo1: TDBMemo
      Left = 312
      Top = 78
      Width = 180
      Height = 92
      DataField = 'name'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBMemo2: TDBMemo
      Left = 18
      Top = 77
      Width = 253
      Height = 120
      DataField = 'description'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBComboBox1: TDBComboBox
      Left = 427
      Top = 176
      Width = 65
      Height = 21
      Style = csDropDownList
      DataField = 'holiday'
      DataSource = DataSource1
      ItemHeight = 13
      Items.Strings = (
        #1576#1604#1607
        #1582#1610#1585)
      TabOrder = 6
    end
  end
  object Panel2: TPanel
    Left = 8
    Top = 8
    Width = 539
    Height = 314
    TabOrder = 4
    Visible = False
    object Bevel2: TBevel
      Left = 11
      Top = 41
      Width = 513
      Height = 2
      Shape = bsBottomLine
    end
    object Label3: TLabel
      Left = 455
      Top = 16
      Width = 69
      Height = 19
      Caption = #1605#1585#1581#1604#1607' '#1583#1608#1605
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 445
      Top = 56
      Width = 48
      Height = 13
      Caption = #1575#1610#1580#1575#1583' '#1705#1606#1606#1583#1607
    end
    object Label5: TLabel
      Left = 445
      Top = 88
      Width = 41
      Height = 13
      Caption = #1606#1575#1605' '#1576#1587#1578#1607
    end
    object Label6: TLabel
      Left = 445
      Top = 120
      Width = 51
      Height = 13
      Caption = #1570#1582#1585#1610#1606' '#1578#1594#1610#1610#1585
    end
    object Label7: TLabel
      Left = 445
      Top = 152
      Width = 43
      Height = 13
      Caption = #1606#1608#1593' '#1578#1602#1608#1610#1605
    end
    object Label8: TLabel
      Left = 445
      Top = 176
      Width = 75
      Height = 13
      Caption = #1583#1585#1576#1575#1585#1607' '#1578#1608#1586#1610#1593' '#1705#1606#1606#1583#1607
    end
    object authorEdt: TEdit
      Left = 254
      Top = 53
      Width = 185
      Height = 21
      TabOrder = 0
    end
    object dbNameEdt: TEdit
      Left = 254
      Top = 85
      Width = 185
      Height = 21
      TabOrder = 1
    end
    object lastChangeYearEdt: TEdit
      Left = 403
      Top = 117
      Width = 36
      Height = 21
      TabOrder = 2
    end
    object providerMemo: TMemo
      Left = 11
      Top = 176
      Width = 428
      Height = 129
      TabOrder = 3
    end
    object lastChangeMonthEdt: TEdit
      Left = 370
      Top = 117
      Width = 27
      Height = 21
      TabOrder = 4
    end
    object lastChangeDayEdt: TEdit
      Left = 337
      Top = 117
      Width = 27
      Height = 21
      TabOrder = 5
    end
    object calTypeEdt: TComboBox
      Left = 254
      Top = 149
      Width = 185
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 6
      Text = #1580#1604#1575#1604#1610' ('#1607#1580#1585#1610' '#1588#1605#1587#1610')'
      Items.Strings = (
        #1580#1604#1575#1604#1610' ('#1607#1580#1585#1610' '#1588#1605#1587#1610')'
        #1607#1580#1585#1610' '#1602#1605#1585#1610
        #1605#1610#1604#1575#1583#1610' ('#1711#1585#1711#1608#1585#1610#1606')')
    end
    object Button3: TButton
      Left = 304
      Top = 117
      Width = 27
      Height = 21
      Hint = #1575#1605#1585#1608#1586
      Caption = '...'
      TabOrder = 7
      OnClick = Button3Click
    end
    object Button2: TButton
      Left = 11
      Top = 49
      Width = 86
      Height = 25
      Caption = #1576#1575#1585#1711#1586#1575#1585#1610' '#1605#1580#1583#1583
      TabOrder = 8
      OnClick = Button2Click
    end
  end
  object Button1: TButton
    Left = 7
    Top = 328
    Width = 75
    Height = 25
    Caption = #1582#1585#1608#1580
    TabOrder = 1
    OnClick = Button1Click
  end
  object backBtn: TButton
    Left = 175
    Top = 328
    Width = 75
    Height = 25
    Caption = #1602#1576#1604#1610
    Enabled = False
    TabOrder = 2
    OnClick = backBtnClick
  end
  object nextBtn: TButton
    Left = 94
    Top = 328
    Width = 75
    Height = 25
    Caption = #1576#1593#1583#1610
    TabOrder = 3
    OnClick = nextBtnClick
  end
  object OpenDialog1: TOpenDialog
    Filter = '*.s3db|*.s3db'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Left = 336
    Top = 313
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.s3db'
    Filter = '*.s3db|*.s3db'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofNoReadOnlyReturn, ofEnableSizing]
    Left = 368
    Top = 313
  end
  object LiteDB: TASQLite3DB
    TimeOut = 0
    CharacterEncoding = 'UTF8'
    Database = '.s3db'
    DefaultExt = '.s3db'
    Version = '3.5.1'
    DriverDLL = 'sqlite3.dll'
    Connected = False
    MustExist = False
    ExecuteInlineSQL = False
    Left = 408
    Top = 313
  end
  object LiteQuery: TASQLite3Query
    AutoCommit = False
    SQLiteDateFormat = True
    Connection = LiteDB
    MaxResults = 0
    StartResult = 0
    TypeLess = False
    SQLCursor = True
    ReadOnly = False
    UniDirectional = False
    RawSQL = False
    Left = 440
    Top = 313
  end
  object LiteTableEvents: TASQLite3Table
    AutoCommit = False
    SQLiteDateFormat = True
    Connection = LiteDB
    MaxResults = 0
    StartResult = 0
    TypeLess = False
    SQLCursor = True
    ReadOnly = False
    UniDirectional = False
    TableName = 'events'
    PrimaryAutoInc = True
    Left = 472
    Top = 313
    object LiteTableEventsid: TIntegerField
      DefaultExpression = '1'
      FieldKind = fkInternalCalc
      FieldName = 'id'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey, pfHidden]
      ReadOnly = True
    end
    object LiteTableEventsmonth: TStringField
      FieldName = 'month'
      KeyFields = 'id'
      Size = 255
    end
    object LiteTableEventsday: TStringField
      FieldName = 'day'
      KeyFields = 'id'
      Size = 255
    end
    object LiteTableEventsname: TMemoField
      FieldName = 'name'
      KeyFields = 'id'
      OnGetText = LiteTableEventsnameGetText
      BlobType = ftMemo
    end
    object LiteTableEventsdescription: TMemoField
      FieldName = 'description'
      KeyFields = 'id'
      OnGetText = LiteTableEventsdescriptionGetText
      BlobType = ftMemo
    end
    object LiteTableEventsholiday: TMemoField
      FieldName = 'holiday'
      KeyFields = 'id'
      OnGetText = LiteTableEventsholidayGetText
      OnSetText = LiteTableEventsholidaySetText
      BlobType = ftMemo
    end
  end
  object DataSource1: TDataSource
    DataSet = LiteTableEvents
    Enabled = False
    Left = 504
    Top = 313
  end
  object LiteTableDefinition: TASQLite3Table
    AutoCommit = False
    SQLiteDateFormat = True
    Connection = LiteDB
    MaxResults = 0
    StartResult = 0
    TypeLess = False
    SQLCursor = True
    ReadOnly = False
    UniDirectional = False
    AutoCalcFields = False
    TableName = 'db_definition'
    PrimaryAutoInc = False
    Left = 472
    Top = 345
  end
end
